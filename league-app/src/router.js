import Vue from 'vue'
import Router from 'vue-router'
import Search from './components/Search.vue'
import SearchSeries from './components/SearchSeries.vue'
import Credit from './components/Credit.vue'
import DetailMovie from './components/DetailMovie.vue'
import DetailSerie from './components/DetailSerie.vue'
import Connexion from './components/Connexion.vue'
import Inscription from './components/Inscription.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Search',
      component: Search
    },
    {
      path: '/SearchSeries',
      name: 'SearchSeries',
      component: SearchSeries
    },
    {
      path: '/DetailSerie',
      name: 'DetailSerie',
      component: DetailSerie,
      props: true
    },
    {
      path: '/Credit',
      name: 'Credit',
      component: Credit
    },
    {
      path: '/DetailMovie',
      name: 'DetailMovie',
      component: DetailMovie,
      props: true
    },
    {
      path: '/Connexion',
      name: 'Connexion',
      component: Connexion
    },
    {
      path: '/Inscription',
      name: 'Inscription',
      component: Inscription
    }
  ]
})