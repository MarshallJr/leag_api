import Vue from 'vue'
import App from './App.vue'
import 'bulma/css/bulma.css'
import 'buefy/dist/buefy.css'
import router from './router'
import Buefy from 'buefy'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Carousel3d from 'vue-carousel-3d'
import { i18n } from '@/plugins/i18n'
import VueScrollTo from 'vue-scrollto'
import { Trans } from './plugins/Translation'

Vue.prototype.$i18nRoute = Trans.i18nRoute.bind(Trans)

Vue.use(VueAxios, axios)
Vue.use(Buefy)
Vue.use(Carousel3d)
Vue.use(VueScrollTo)

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')
